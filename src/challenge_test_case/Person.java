package challenge_test_case;

public class Person extends BaseClass<Person>{
	
	public String FirstName;
	public String LastName;
	public Address Address;
	
	public Person(String firstName, String lastName, Address address){
		
		this.Id = BaseClass.nextId.incrementAndGet();
		this.FirstName = firstName;
		this.LastName = lastName;
		this.Address = address;
	}

}
