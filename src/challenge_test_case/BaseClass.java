package challenge_test_case;

import java.io.*;
import java.util.concurrent.atomic.AtomicInteger;

public class BaseClass<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int Id;
	static AtomicInteger nextId = new AtomicInteger();
		
	
	public static <T> T Find(int id){		
		
		try {
			FileInputStream fis = new FileInputStream(""+id+"");
			ObjectInputStream ois = new ObjectInputStream(fis);
			BaseClass result = (BaseClass) ois.readObject();
			ois.close();
			return (T) result;
		}
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}	
		 
	}
	
	
	public void Save(){
		try {			
			
			FileOutputStream fos = new FileOutputStream(""+Id);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			//this.lastAssignedId = id;
			oos.writeObject(this);
			oos.close();

			// BELOW FOR DEBUG
			FileInputStream fis = new FileInputStream(""+Id);
			ObjectInputStream ois = new ObjectInputStream(fis);
			BaseClass result = (BaseClass) ois.readObject();
			ois.close();

			System.out.println("One:" + Id);
		}
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	public void Delete(){
					
		File file = new File(""+Id);
		file.delete();
		
	}
	
	/*public static void main(String[] args){
		BaseClass bc = new BaseClass();
		bc.id = 7;
		bc.Save();
	}*/

}
