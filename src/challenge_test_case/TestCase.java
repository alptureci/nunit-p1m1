package challenge_test_case;

public class TestCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Address address = new Address("4875 Sun Tail", "Queen Creek", "TX", "38452");
		Person person = new Person("Bill", "Smith", address);
		Person person2 = new Person("Alp", "Smith", address);
		Company company = new Company("P1M1 Software And Consulting", address);
		
		person.Save();
		company.Save();
		
		Person savedPerson = Person.Find(person.Id);
		
		System.out.println(savedPerson.Id);
		
		person.Delete();
		
		System.out.println(person.Id);

	}

}
