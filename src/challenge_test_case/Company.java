package challenge_test_case;

public class Company extends BaseClass<Company>{
	
	public String Name;
	public Address Address;
	
	public Company(String name, Address address){

		this.Id = BaseClass.nextId.incrementAndGet();
		this.Name = name;
		this.Address = address;
	}

}
