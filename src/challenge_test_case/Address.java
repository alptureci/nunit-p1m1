package challenge_test_case;

public class Address extends BaseClass{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -914084201095305002L;
	
	public String streetAddress;
	public String city;
	public String state;
	public String zip;
	
	public Address(String streetAddress, String city, String state, String zip){
		this.streetAddress = streetAddress;
		this.city = city;
		this.state = state;
		this.zip = zip;
	}
	
	
	public static void main(String[] args){
		//Address address = new Address("4875 Sun Tail", "Queen Creek", "TX", "38452");
		//address.Save();
	}
	

}
